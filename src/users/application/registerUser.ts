import { UserDto } from "./dtos/user.dto";
import { CommonResponse } from "../../shared/infrastructure/common.response";
import { UseCase } from "../../shared/application/base.useCase";

export class RegisterUser implements UseCase{
    
    private user:UserDto
    type: "RegisterUser"
    
    constructor(user:UserDto){
        this.user = user;   
    }

    map2Response(){
        let res = new CommonResponse()
        res.type = this.type
        res.response.description = "Se ha creado el usuario "+this.user.getName()+" con id "+this.user.getUserId()
        return res;
    }

} 