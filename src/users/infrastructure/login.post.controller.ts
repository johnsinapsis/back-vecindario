import { Controller, configControllerIdentity } from "../../controllers/controller.base";
import { ValidateFieldsLogin } from "../application/validations/validateFieldsLogin";
import { SessionPersistence } from "./persistence/session.persistence";
import { ExecRulesLogin } from "./rules/exec.rules.login";
import { BaseErrorHandler } from "../../shared/infrastructure/base.error.handler";
import { ErrorLogin } from "./handler/error.login";
import { LoginUserClient } from "../application/loginUserClient";
import { UserPersistence } from "./persistence/user.persistence";
import { UserMapper } from "../application/mappers/user.mapper";
import { UserDto } from "../application/dtos/user.dto";
import { Authentication } from "../../shared/infrastructure/auhentication";

export class LoginPostController extends Controller implements configControllerIdentity{
    
    private validateFields
    private daoCredentials
    private daoUser
    private rules
    private loginUserClient
    private auth

    constructor(){
        super()
        this.validateFields = new ValidateFieldsLogin()
        this.daoCredentials = new SessionPersistence()
        this.daoUser = new UserPersistence();
        this.rules = new ExecRulesLogin()
        this.auth = new Authentication()
    }
    
    getConfigId(){
        return 'login'
    }

    async getUserDto(email): Promise <UserDto>{
        let user = await this.daoUser.getUserByEmail(email)
        let userMapper = new UserMapper()
        return userMapper.map2UserDto(user)
    }

    async postAuthenticate(req,res){
        try{
            if(this.validateFields.isValid(req,res)){
                let {email,password} = req.body
                let params = {email,password}
                await this.rules.exec(params)
                let session = await this.daoCredentials.createSession(params)
                let userDto:UserDto = await this.getUserDto(params.email)
                let signature = this.auth.sign(userDto,session)
                //console.log(signature);
                this.loginUserClient = new LoginUserClient(signature)
                let response = this.loginUserClient.map2Response()
                return res.json(response)
            }
            else{
                let error = new ErrorLogin()
                return res.json(error.buildResponse(res))
            }
        }catch(e){
            console.log(e);
            return BaseErrorHandler.handle(res,e)
        }
    }
}