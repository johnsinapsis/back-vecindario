import { ExpressServer } from "./server/server";

class Init {
    server
    rMng

    constructor(){
        this.server = new ExpressServer()
        this.rMng = this.server.getRouteManager()
    }
    
    registerRoutes(){
        this.rMng.bindAny('LoginPostController')
        this.rMng.bindAny('ShowAllProductsByUserGetController')
        this.rMng.bindAny('ShowAllTransactionsByProductPostController')
        this.rMng.bindAny('ShowDetailTransactionGetController')
        this.rMng.bindAny('RequestProductPostController')
        this.rMng.bindAny('RegisterUserPostController')
        this.rMng.bindAny('getAllPostsController')
        this.rMng.bindAny('AddActionUserInPost')
    }

}

let init = new Init()
init.registerRoutes()
init.server.startup()