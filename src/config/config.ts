import jp from 'jsonpath'
import dotenv from 'dotenv'

dotenv.config()

const config = {
    port: process.env.node_port || 3002,
    ep:{
        login:{
            postAuthenticate:'/login'
        },
        posts:{
            getPosts:'/posts'
        },
        postsActions:{
            putAddActions:'/post/actions'
        },
        products:{
            getAllProductsByUser:'/products/:userId'
        },
        transactions:{
            postAllTransactionsByProduct: '/products/transactions'
        },
        transaction:{
            getDetailTransaction: '/products/transaction/:transactionId',
        },
        
        requestProduct:{
            postRequestProduct:'/products'
        },
        registerUser:{
            postRegister:'/user/register'
        }
    },
    db:{
        motor: process.env.db_motor+'' || 'mariadb',
        host: process.env.db_host || 'localhost',
        port: process.env.db_port || 3306,
        database: process.env.db_database || 'dreams_bank',
        user: process.env.db_user || 'root',
        pass: process.env.db_pass || ''
    },
    auth:{
        token:process.env.api_key || 'johnsinapsis',
        time: process.env.time_token || 30
    }
}

export default config

export class EnvConfig {
    static pathSeparator() {
        let os = process.platform
        if (os.startsWith("win")) {
            return "\\"
        } else {
            return "/"
        }
    }
}


export class ConfigReader {
    static getPathsMap(controllerId) {
        return jp.query(config, 'ep.' + controllerId)[0]
    }
}