import axios from 'axios'
import { BaseUseCase } from './base.useCase'

export class UpdateLikeOrDislikePostUseCase extends BaseUseCase{
    
    private route = "/posts/actions"
    private request = {}
    
    constructor(){
        super()
    }

    init(request){
        this.request = request
    }

    async test(){
        let route = this.url+this.route
        let res = await axios.put(route,this.request)
        return res.data
    }
}