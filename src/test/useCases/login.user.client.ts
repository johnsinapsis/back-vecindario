import axios from 'axios'
import { BaseUseCase } from './base.useCase'

export class LoginUserClientUseCase extends BaseUseCase{

    private route = "/login"
    private request = {}
    constructor(){
        super()
    }

    init(documentNumber,password){
        this.request = {documentNumber,password}
    }

    async test(){
        let route = this.url+this.route
        let res = await axios.post(route,this.request)
        return res.data
    }
}