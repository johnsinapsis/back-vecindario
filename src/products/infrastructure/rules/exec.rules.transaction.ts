import { TransactionPersistence } from "../persistence/transaction.persistence";
import { BusinessError } from "../../../shared/application/handler/business.error";
import { BusinessTransaction } from "../../../shared/application/business.rules";

export class ExecRulesTransaction implements BusinessTransaction{
    private type = "ExecRulesProduct"
    private daoTransaction

    constructor(){
        this.daoTransaction = new TransactionPersistence()
    }

    public async existTransaction(transactionId:number): Promise <boolean>{
        let transaction = await this.daoTransaction.getTransaction(transactionId)
        if(!transaction.id)
            return false
        return true
    }

    public async exec(userId:number){
        if(! await this.existTransaction(userId))
            this.throwBusiness("La transacción no existe")
    }

    public throwBusiness(msg){
        let response = {error: msg}
        throw new BusinessError(this.type,response)
    }
}