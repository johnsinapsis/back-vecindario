import { BusinessError } from "../../../shared/application/handler/business.error";
import { ExecRulesTransaction } from "./exec.rules.transaction";

export class ExecRulesGetTransaction {

    private type = "ExecRulesGetTransaction"
    private rulesTransaction

    constructor(){
        this.rulesTransaction = new ExecRulesTransaction()
    }

    public async exec(transactionId:number){
        await this.rulesTransaction.exec(transactionId)
    }

}