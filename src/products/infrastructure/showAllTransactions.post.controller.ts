import { Controller, configControllerIdentity } from "../../controllers/controller.base";
import { BaseErrorHandler } from "../../shared/infrastructure/base.error.handler";
import { ValidateFieldsAllTransactionsByProduct } from "../application/validations/validateFieldsAllTransactions";
import { ErrorGetAllTransactionsByProduct } from "./handler/error.get.all.transactions";
import { ExecRulesGetAllTransactionByProduct } from "./rules/exec.rules.getAllTransactions";
import { TransactionPersistence } from "./persistence/transaction.persistence";
import { GetAllTransactionsByProduct } from "../application/getAllTransactionsByProduct";

export class ShowAllTransactionsByProductPostController extends Controller implements configControllerIdentity{
    
    private validateFields
    private rules
    private daoTransactions
    private getTransactionsByProduct

    constructor(){
        super()
        this.validateFields = new ValidateFieldsAllTransactionsByProduct()
        this.rules = new ExecRulesGetAllTransactionByProduct()
        this.daoTransactions = new TransactionPersistence()
    }

    getConfigId(){
        return 'transactions'
    }

    async postAllTransactionsByProduct(req,res){
        try{
            if(this.validateFields.isValid(req,res)){
                let {userId,productId} = req.body
                let params = {userId,productId}
                await this.rules.exec(params)
                let transactions = await this.daoTransactions.getTransactionsByProduct(productId)
                this.getTransactionsByProduct = new GetAllTransactionsByProduct(transactions)
                return res.json(this.getTransactionsByProduct.map2Response())
            }
            else{
                let error = new ErrorGetAllTransactionsByProduct()
                return res.json(error.buildResponse(res))
            }
        }catch(e){
            return BaseErrorHandler.handle(res,e)
        }
    }
}