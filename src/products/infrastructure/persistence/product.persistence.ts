import { sequelize } from "../../../shared/infrastructure/base.persistence";
import { ProductRepository } from "../../application/repositories/product.repository";
import { Product } from "../../domain/product";
import { QueryTypes } from "sequelize"
import { ProductModel } from "./models/product.model";

export class ProductPersistence implements ProductRepository{
    public async getProductsByUser(userId:number):Promise <Array <Object>>{
        let sql = "SELECT a.id, b.name, a.account_number, state FROM products a INNER JOIN product_types b ON a.`type` = b.id WHERE a.user_id = ?"
        const products: any = await sequelize.query(sql, 
            { 
                replacements: [userId],
                type: QueryTypes.SELECT 
            });
        return products
    }

    public async getProduct(productId:number): Promise <Product>{
        const products: any = await sequelize.query("SELECT * FROM products where id = ?", 
            { 
                replacements: [productId],
                type: QueryTypes.SELECT 
            });
        return this.map2response(products)
    }

    public async getProductByAccount(accountNumber:string): Promise <Product>{
        const products: any = await sequelize.query("SELECT * FROM products where account_number = ?", 
        { 
            replacements: [accountNumber],
            type: QueryTypes.SELECT 
        });
        return this.map2response(products)
    }

    public async createProduct(req:any): Promise <Product>{
        let productModel = new ProductModel()
        const newProduct = await productModel.product.create({
            type: req.type,
            accountNumber: req.accountNumber,
            userId: req.userId,
            state: req.state
        })
        let product:Product = newProduct.dataValues
        return product
    }

    private map2response(products:any): Product{
        if(products.length===0)
            return new Product
        let product:Product = {
            id: products[0].id,
            type: products[0].type,
            accountNumber: products[0].account_number,
            userId: products[0].user_id,
            state: products[0].state,
        }
        return product
    }
}