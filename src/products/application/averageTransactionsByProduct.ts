import { UseCase } from "../../shared/application/base.useCase";
import { CommonResponse } from "../../shared/infrastructure/common.response";

export class AverageTransactionsByProduct implements UseCase{
    private average: number
    type = "AverageTransactionsByProduct"
    constructor(average:number){
        this.average = average
    }
    map2Response(){
        let res = new CommonResponse()
        res.type = this.type
        res.response.description = "Consulta realizada correctamente"
        res.response["average"] = this.average
        return res
    }
}

