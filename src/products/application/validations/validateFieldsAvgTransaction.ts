import { BaseValidation } from "../../../validation/base.validation";

export class ValidateFieldsAvgTransaction extends BaseValidation{
    constructor(){
        super()
        let emptyUser = this.fieldValidationFactory.createInstance('empty','$.userId','El id del usuario es requerido')
        let emptyProduct = this.fieldValidationFactory.createInstance('empty','$.productId','El id del producto es requerido')
        let emptyDateStart = this.fieldValidationFactory.createInstance('empty','$.dateStart','La fecha de inicio del rango es requerida')
        let emptyDateEnd = this.fieldValidationFactory.createInstance('empty','$.dateEnd','La fecha final del rango es requerida')
        let numberUserId = this.fieldValidationFactory.createInstance('number', '$.userId', 'El id del usuario debe ser numérico');
        let numberProductId = this.fieldValidationFactory.createInstance('number', '$.productId', 'El id del producto debe ser numérico');
        let dateDateStart = this.fieldValidationFactory.createInstance('date','$.dateStart','La fecha de inicio del rango debe ser de formato YYYY-MM-DD')
        let dateDateEnd = this.fieldValidationFactory.createInstance('date','$.dateEnd','La fecha final del rango debe ser de formato YYYY-MM-DD')
        let rangeDate = this.fieldValidationFactory.createInstance('range-date','$','La fecha final debe ser mayor a la inicial')
        this.addValidator(emptyDateStart)
        this.addValidator(emptyDateEnd)
        this.addValidator(emptyUser)
        this.addValidator(emptyProduct)
        this.addValidator(numberUserId)
        this.addValidator(numberProductId)
        this.addValidator(dateDateStart)
        this.addValidator(dateDateEnd)
        this.addValidator(rangeDate)
    }
}