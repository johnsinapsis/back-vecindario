export class FormatFields{
    static formatDateStart(date){
        return date+' 00:00'
    }

    static formatDateEnd(date){
        return date+' 23:59'
    }
}