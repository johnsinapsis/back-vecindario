import jwt from 'jsonwebtoken'
import config  from '../../config/config'
import { UserDto } from '../../users/application/dtos/user.dto'
import { Session } from '../../users/domain/session'
import { BusinessError } from '../application/handler/business.error'

export class Authentication{

    private apiKey = config.auth.token
    private time = config.auth.time+'d'
    private type = "ErrorAuthentication"

    public sign(user:UserDto,session:Session){
        return jwt.sign({user,session},this.apiKey,{expiresIn:this.time});
    }

    public validateToken(token,res){
        let decoded =jwt.verify(token,this.apiKey,(error,decoded)=>{
            if(error){
                this.throwBusiness("Token de autenticación no válido")
            }
            return decoded
        })
        return decoded
    }

    public throwBusiness(msg){
        let response = {error: msg}
        throw new BusinessError(this.type,response)
    }

}