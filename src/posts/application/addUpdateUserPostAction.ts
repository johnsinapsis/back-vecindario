import { UseCase } from "../../shared/application/base.useCase";
import { UserPostActionDto } from "./dtos/userPostAction.dto";
import { UserPostActionPersistence } from "../infrastructure/persistence/userPostAction.persistence";
import { UserPostActionMapper } from "./mappers/userPostAction.mapper";
 
export class AddUpdateUserPostAction implements UseCase {
    
    public type= "AddUpdateUserPostAction";
    private dto:UserPostActionDto
    private newRegister = false
    private action:string
    private daoUserPostAction

    constructor(dto:UserPostActionDto,{action,userId,postId}){
        this.daoUserPostAction = new UserPostActionPersistence()
        if(!dto.getId()){
            this.newRegister = true
            this.dto = new UserPostActionDto()
            this.dto.setUserId(userId)
            this.dto.setPostId(postId)
            this.dto.setDislike(0)
            this.dto.setLike(0)
            if(action==='likes')
                this.dto.setLike(1)
            else
                this.dto.setDislike(1)
        }
        else{
            this.dto = dto
        }
        this.action = action
    }

    public async setUserPostAction(){
        if(this.newRegister){
            let actionMapper = new UserPostActionMapper()
            let register:UserPostActionDto = 
            await this.daoUserPostAction.createAction(actionMapper.map2Entity(this.dto))
            return register ? 1 : 0
        }
        else{
            let numRegister = 0
            if(this.action==='likes'){
                if(this.dto.getLike()===1){
                    this.dto.setLike(0)
                    numRegister = -1
                }
                else{
                    this.dto.setLike(1)
                    numRegister = 1
                }
            }
            else{
                if(this.dto.getDislike()===1){
                    this.dto.setDislike(0)
                    numRegister = -1
                }
                else{
                    this.dto.setDislike(1)
                    numRegister = 1
                }
            }
            let register:UserPostActionDto =
                await this.daoUserPostAction.updateAction(this.dto)

            return register ? numRegister : 0
        }
    }

    map2Response(): Object {
        throw new Error("Method not implemented.");
    }
    
}