import { PostDto } from "../dtos/post.dto";

export interface PostRepository{
    getPosts():Promise<Array <PostDto>>
    updatePost(post:PostDto):Promise <PostDto>
    getPostById(postId:number): Promise <PostDto>
}