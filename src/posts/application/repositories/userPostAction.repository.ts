import { UserPostActionDto } from "../dtos/userPostAction.dto";
import { UserPostAction } from "../../domain/userPostAction";

export interface UserPostActionRepository{
    createAction(action:UserPostAction):Promise <UserPostActionDto>
    getActionByUserAndPost(userId:number,postId:number):Promise <UserPostActionDto>
    updateAction(actionDto:UserPostActionDto):Promise <UserPostActionDto>
}