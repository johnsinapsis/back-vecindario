export class PostDto{
    private id: number
    private date:string
    private userId:number
    private title:string
    private body:string
    private picture:string
    private likes:number
    private dislikes:number

    public getId(): number {
        return this.id;
    }

    public setId(id: number): void {
        this.id = id;
    }

    public getDate(): string {
        return this.date;
    }

    public setDate(date: string): void {
        this.date = date;
    }

    public getUserId(): number {
        return this.userId;
    }

    public setUserId(userId: number): void {
        this.userId = userId;
    }

    public getTitle(): string {
        return this.title;
    }

    public setTitle(title: string): void {
        this.title = title;
    }

    public getBody(): string {
        return this.body;
    }

    public setBody(body: string): void {
        this.body = body;
    }

    public getPicture(): string {
        return this.picture;
    }

    public setPicture(picture: string): void {
        this.picture = picture;
    }

    public getLikes(): number {
        return this.likes;
    }

    public setLikes(likes: number): void {
        this.likes = likes;
    }

    public getDislikes(): number {
        return this.dislikes;
    }

    public setDislikes(dislikes: number): void {
        this.dislikes = dislikes;
    }

}