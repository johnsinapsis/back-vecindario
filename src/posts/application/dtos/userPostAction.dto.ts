export class UserPostActionDto{
    private id:number
    private userId:number
    private postId:number
    private like:number
    private dislike:number

    public getId(): number {
        return this.id;
    }

    public setId(id: number): void {
        this.id = id;
    }

    public getUserId(): number {
        return this.userId;
    }

    public setUserId(userId: number): void {
        this.userId = userId;
    }

    public getPostId(): number {
        return this.postId;
    }

    public setPostId(postId: number): void {
        this.postId = postId;
    }

    public getLike(): number {
        return this.like;
    }

    public setLike(like: number): void {
        this.like = like;
    }

    public getDislike(): number {
        return this.dislike;
    }

    public setDislike(dislike: number): void {
        this.dislike = dislike;
    }

}