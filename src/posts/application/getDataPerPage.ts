import { UseCase } from "../../shared/application/base.useCase";
import { Utils } from "../../shared/application/utilities";

export class GetDataPerPage implements UseCase{
    type = "GetDataPerPage";
    private posts
    private totalPages
    private page
    private registersPerPage
    private totalRegs

    constructor(posts){
        this.posts = posts
    }

    getData(req){
        if(!this.validateParams(req))
        return this.posts
        this.totalPages = Math.ceil(this.totalRegs/this.registersPerPage) 
        console.log(this.totalPages,this.page);
        if(this.page>this.totalPages)
            return []
        let start = (this.page * this.registersPerPage) - this.registersPerPage
        let end = (this.page * this.registersPerPage) 
        let filterPostByPage = this.posts.slice(start,end)
        return filterPostByPage
    }

    private validateParams(req){
        if(!req.page)
            return false
        if(!Utils.isNumber(req.page))
            return false
        if(!Utils.isNumber(req.registersPerPage))
            return false 
        if(req.page===0 || req.registersPerPage===0)
            return false
        this.page = req.page
        this.totalRegs = this.posts.length
        this.registersPerPage = req.registersPerPage
        if(this.totalRegs<this.page)
            return false
        if(this.totalRegs<=this.registersPerPage)
            return false
        return true
    }

    private

    map2Response(): Object {
        throw new Error("Method not implemented.");
    }
    
}