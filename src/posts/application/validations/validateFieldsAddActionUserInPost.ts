import { BaseValidation } from "../../../validation/base.validation";

export class ValidateFieldsAddActionUserInPost extends BaseValidation{
    constructor(){
        super()
        let emptyPost = this.fieldValidationFactory.createInstance('empty','$.postId','El id del post es requerido')
        let emptyAction = this.fieldValidationFactory.createInstance('empty','$.action','El campo action es requerido')
        let numberPost = this.fieldValidationFactory.createInstance('number', '$.postId', 'El id del post debe ser numérico');
        this.addValidator(emptyPost)
        this.addValidator(emptyAction)
        this.addValidator(numberPost)
    }
}