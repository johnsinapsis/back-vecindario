import { CommonResponse } from "../../shared/infrastructure/common.response";
import { UseCase } from "../../shared/application/base.useCase";
import { PostDto } from "./dtos/post.dto";

export class ActionUserInPost implements UseCase{

    private post:PostDto
    type = "LikeAndDislakeActionInPost"
    
    constructor(post:PostDto){
        this.post = post
    }

    map2Response(){
        let res = new CommonResponse()
        res.type = this.type
        res.response.description = "Se ha actualizado el numero de likes/dislikes correctamente"
        res.response["post"] = this.post
        return res
    }
}