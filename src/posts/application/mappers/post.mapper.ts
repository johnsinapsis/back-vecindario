import { PostDto } from "../dtos/post.dto";
import { Post } from "../../domain/post";
import { Mapper } from "../../../shared/application/mapper";

export class PostMapper extends Mapper{

    public map2RequestDto(post:Post){
        let postDto = new PostDto();
        postDto.setId(post.id)
        postDto.setUserId(post.userId)
        postDto.setTitle(post.title)
        postDto.setBody(post.body)
        postDto.setPicture(post.picture)
        postDto.setDate(post.date)
        postDto.setLikes(post.likes)
        postDto.setDislikes(post.dislikes)
        return postDto
    }

    
    public map2Entity(req:any){
        let post = new Post();
        post.userId = req.userId
        post.date = req.date
        post.title = req.title
        post.body = req.body
        post.picture = req.picture
        post.likes = req.likes
        post.dislikes = req.dislikes
        return post
    }
}