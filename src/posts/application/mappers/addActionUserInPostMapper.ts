import { PostDto } from "../dtos/post.dto";
import { Post } from "../../domain/post";
import { Mapper } from "../../../shared/application/mapper";
import { PostPersistence } from "../../infrastructure/persistence/post.persistence";

export class AddActionUserInPostMapper extends Mapper{
    public async  map2RequestDto(req:any){
        let daoPos:PostPersistence = new PostPersistence()
        let dto:PostDto = await daoPos.getPostById(req.postId) 
        if(req.action==='likes')
            dto.setLikes(dto.getLikes()+req.count)
        if(req.action==='dislikes')
            dto.setDislikes(dto.getDislikes()+req.count)
        return dto
    }
}