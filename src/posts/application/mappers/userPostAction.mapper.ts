import { UserPostActionDto } from "../dtos/userPostAction.dto";
import { UserPostAction } from "../../domain/userPostAction";
import { Mapper } from "../../../shared/application/mapper";

export class UserPostActionMapper extends Mapper{

    public map2RequestDto(action:UserPostAction){
        let postDto = new UserPostActionDto();
        postDto.setId(action.id)
        postDto.setUserId(action.userId)
        postDto.setPostId(action.postId)
        postDto.setLike(action.like)
        postDto.setDislike(action.dislike)
        return postDto
    }

    public map2Entity(req:any){
        let action = new UserPostAction();
        action.userId = req.userId
        action.postId = req.postId
        action.like = req.like
        action.dislike = req.dislike
        return action
    }
}