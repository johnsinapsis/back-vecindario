import { BusinessActionUserInPost } from "../../../shared/application/business.rules";
import { BusinessError } from "../../../shared/application/handler/business.error";
//existAction

export class ExecRulesActionUserType implements BusinessActionUserInPost{
    
    private type = "ExecRulesActionPost"

    existAction(action:string): boolean{
        if((action!=="likes")&&(action!=="dislikes"))
            return false
        return true
    }

    public exec(action:string){
        if(! this.existAction(action))
            this.throwBusiness("opcion del campo  action no válida (likes/dislikes)")
    }

    public throwBusiness(msg){
        let response = {error: msg}
        throw new BusinessError(this.type,response)
    }
}