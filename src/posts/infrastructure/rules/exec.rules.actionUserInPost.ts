import { ExecRulesActionUserType } from "./exec.rules.actionUserType"; 

export class ExecRulesActionUserInPost{

    private type = "ExecRulesRequestProduct"
    private rulesActionUser

    constructor(){
        this.rulesActionUser = new ExecRulesActionUserType()
    }

    public exec(req:any){
        this.rulesActionUser.exec(req.action)
    }

}