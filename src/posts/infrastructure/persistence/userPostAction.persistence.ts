import { sequelize } from "../../../shared/infrastructure/base.persistence";
import { QueryTypes } from "sequelize"
import { UserPostAction } from "../../domain/userPostAction";
import { UserPostActionDto } from "../../application/dtos/userPostAction.dto";
import { UserPostActionModel } from "./models/userPostAction.model";
import { UserPostActionRepository } from "../../application/repositories/userPostAction.repository";
import { UserPostActionMapper } from "../../application/mappers/userPostAction.mapper";

export class UserPostActionPersistence implements UserPostActionRepository{
    public async createAction(action: UserPostAction): Promise<UserPostActionDto> {
        let model = new UserPostActionModel()
        const newAction = await model.entity.create({
            userId: action.userId,
            postId: action.postId,
            like: action.like,
            dislike: action.dislike,
        });
        let actionRegistered:UserPostAction = newAction.dataValues
        let mapper = new UserPostActionMapper();
        return mapper.map2RequestDto(actionRegistered)
    }
    public async getActionByUserAndPost(userId: number, postId: number): Promise<UserPostActionDto> {
        let sql = "SELECT * from user_post_actions where user_id = ? and post_id = ?"
        const actions: any = await sequelize.query(sql,
            {
                replacements: [userId,postId],
                type: QueryTypes.SELECT 
            })
        
            return this.map2response(actions,0)
    }
    public async updateAction(actionDto: UserPostActionDto): Promise<UserPostActionDto> {
        let mapper = new UserPostActionMapper()
        let action:UserPostAction = mapper.map2Entity(actionDto)
        let model = new UserPostActionModel()
        const updateAction = await model.entity.update(action,{where:{id:actionDto.getId()}})
        return actionDto
    }

    private map2response(actions:any, pos): UserPostActionDto{
        if(actions.length===0)
            return new UserPostActionDto
        let dto:UserPostActionDto = new UserPostActionDto()
        dto.setId(actions[pos].id)
        dto.setUserId(actions[pos].user_id)
        dto.setPostId(actions[pos].post_id)
        dto.setLike(actions[pos].like)
        dto.setDislike(actions[pos].dislike)
        return dto
    }
    
} 