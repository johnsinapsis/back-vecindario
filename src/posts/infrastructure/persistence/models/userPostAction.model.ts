import { Sequelize, DataTypes, Model } from 'sequelize'
import { values } from 'sequelize/types/lib/operators'
import { sequelize } from '../../../../shared/infrastructure/base.persistence'

export class UserPostActionModel{
    
    public entity

    constructor(){
        this.entity = sequelize.define('userPostAction',{
            id: {
                type: DataTypes.INTEGER,
                autoIncrement:true,
                allowNull: false,
                primaryKey: true
            },
            userId: {
                type: DataTypes.INTEGER,
                field: 'user_id'
            },
            postId: {
                type: DataTypes.INTEGER,
                field: 'post_id'
            },
            like: {
                type: DataTypes.INTEGER,
            },
            dislike: {
                type: DataTypes.INTEGER,
            }
        },
        {
            tableName: 'user_post_actions',
            timestamps: false
        })
    }
}