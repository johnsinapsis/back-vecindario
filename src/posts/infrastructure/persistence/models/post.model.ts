import { Sequelize, DataTypes, Model } from 'sequelize'
import { values } from 'sequelize/types/lib/operators'
import { sequelize } from '../../../../shared/infrastructure/base.persistence'

export class PostModel{

    public entity

    constructor(){
        this.entity = sequelize.define('post',{
            id: {
                type: DataTypes.INTEGER,
                autoIncrement:true,
                allowNull: false,
                primaryKey: true
            },
            userId: {
                type: DataTypes.INTEGER,
                field: 'user_id'
            },
            title: {
                type: DataTypes.STRING,
                field: 'title',
            },
            body: {
                type: DataTypes.STRING,
            },
            picture: {
                type: DataTypes.STRING,
            },
            likes: {
                type: DataTypes.INTEGER,
            },
            dislikes: {
                type: DataTypes.INTEGER,
            }
        },
        {
            timestamps: false
        })
    }
}