import { sequelize } from "../../../shared/infrastructure/base.persistence";
import { PostRepository } from "../../application/repositories/post.repository";
import { Post } from "../../domain/post";
import { PostDto } from "../../application/dtos/post.dto";
import { QueryTypes } from "sequelize"
import { PostMapper } from "../../application/mappers/post.mapper";
import { PostModel } from "./models/post.model";

export class PostPersistence implements PostRepository{

    public async getPosts(): Promise <Array <PostDto>>{
        let sql = "SELECT * from posts order by created_at desc"
        const posts: any = await sequelize.query(sql,{type: QueryTypes.SELECT })
        let arrayPost = []
        for(let i=0; i<posts.length; i++){
            let row:PostDto = this.map2response(posts,i)
            arrayPost.push(row)
        }
        return arrayPost
    }

    public async getPostById(postId:number):Promise <PostDto>{
        let sql = "select * from posts where id = ?"
        const posts: any = await sequelize.query(sql,
            {
                replacements: [postId],
                type: QueryTypes.SELECT 
            })
        return this.map2response(posts,0)
    }

    public async updatePost(postDto:PostDto): Promise  <PostDto>{
        let mapper = new PostMapper()
        let post:Post = mapper.map2Entity(postDto)
        let model = new PostModel()
        const updatePost = await model.entity.update(post,{where:{id:postDto.getId()}})
        console.log(updatePost);
        return postDto
    }

    private map2response(posts:any, pos): PostDto{
        if(posts.length===0)
            return new PostDto
        let postDto:PostDto = new PostDto()
        postDto.setId(posts[pos].id)
        postDto.setUserId(posts[pos].user_id)
        postDto.setTitle(posts[pos].title)
        postDto.setBody(posts[pos].body)
        postDto.setPicture(posts[pos].picture)
        postDto.setDate(posts[pos].created_at)
        postDto.setLikes(posts[pos].likes)
        postDto.setDislikes(posts[pos].dislikes)
        return postDto
    }
}