import { Controller, configControllerIdentity } from "../../controllers/controller.base";
import { BaseErrorHandler } from "../../shared/infrastructure/base.error.handler";
import { PostPersistence } from "./persistence/post.persistence"
import { GetDataPerPage } from "../application/getDataPerPage";

export class getAllPostsController extends Controller implements configControllerIdentity{

    private daoPosts

    constructor(){
        super()
        this.daoPosts = new PostPersistence()
    }

    getConfigId(){
        return 'posts'
    }

    async getPosts(req,res){
        try{
            let posts = await this.daoPosts.getPosts();
            if(req.query){
                //console.log(req.query);
                let getDataPerPage = new GetDataPerPage(posts)
                posts = getDataPerPage.getData(req.query)
            }
            return res.json(posts)
        }
        catch(e){
            console.log(e);
            return BaseErrorHandler.handle(res,e)
        }
    }
}