import { Controller, configControllerIdentity } from "../../controllers/controller.base";
import { BaseErrorHandler } from "../../shared/infrastructure/base.error.handler";
import { PostPersistence } from "./persistence/post.persistence"
import { UserPostActionPersistence } from "./persistence/userPostAction.persistence";
import { ValidateFieldsAddActionUserInPost } from "../application/validations/validateFieldsAddActionUserInPost";
import { ErrorAddActionUserInPost } from "./handler/error.addActionUserInPost";
import { AddActionUserInPostMapper } from "../application/mappers/addActionUserInPostMapper";
import { PostDto } from "../application/dtos/post.dto";
import { UserPostActionDto } from "../application/dtos/userPostAction.dto";
import { ExecRulesActionUserInPost } from "./rules/exec.rules.actionUserInPost";
import { ActionUserInPost } from "../application/actionUserInPost"; 
import { Authentication } from "../../shared/infrastructure/auhentication";
import { AddUpdateUserPostAction } from "../application/addUpdateUserPostAction";

export class AddActionUserInPost extends Controller implements configControllerIdentity{
    
    private validateFields
    private rules
    private daoPosts
    private daoUserPostAction
    private mapper
    private actionUserInPost
    private auth

    constructor(){
        super()
        this.validateFields = new ValidateFieldsAddActionUserInPost()
        this.rules = new ExecRulesActionUserInPost()
        this.mapper = new AddActionUserInPostMapper()
        this.daoPosts = new PostPersistence()
        this.daoUserPostAction = new UserPostActionPersistence()
        this.auth = new Authentication()
    }

    getConfigId(){
        return 'postsActions'
    }

    async putAddActions(req,res){
        try{
            if(this.validateFields.isValid(req,res)){
                this.rules.exec(req.body)
                let userAuth = this.auth.validateToken(req.get('Authorization'),res);
                //validar si el usuario ya había dado like
                let actionUser:UserPostActionDto = 
                    await this.daoUserPostAction.getActionByUserAndPost(userAuth.user.userId,req.body.postId) 
                let params:any = {...req.body, count:0}
                let addUpdateUserPostAction = new AddUpdateUserPostAction(actionUser,
                    {
                    action: params.action,
                    userId: userAuth.user.userId,
                    postId: params.postId
                })
                params.count = await addUpdateUserPostAction.setUserPostAction()
                let dto:PostDto = await this.mapper.map2RequestDto(params)
                //console.log(dto);
                let updated:PostDto = await this.daoPosts.updatePost(dto)
                this.actionUserInPost = new ActionUserInPost(dto)
                res.json(this.actionUserInPost.map2Response())
            }
            else{
                let error = new ErrorAddActionUserInPost()
                return res.json(error.buildResponse(res))
            }
        }
        catch(e){
            //console.log(e);
            return BaseErrorHandler.handle(res,e)
        }
    }

}