export class Post{
    id: number
    date:string
    userId:number
    title:string
    body:string
    picture:string
    likes:number
    dislikes:number
}