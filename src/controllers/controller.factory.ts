import { LoginPostController } from "../users/infrastructure/login.post.controller";
import { ShowAllProductsByUserGetController } from "../products/infrastructure/showAllProducts.get.controller";
import { ShowAllTransactionsByProductPostController } from "../products/infrastructure/showAllTransactions.post.controller";
import { ShowDetailTransactionGetController } from "../products/infrastructure/showDetailTransaction.get.controller";
import { RequestProductPostController } from "../products/infrastructure/requestProduct.post.controller";
import { RegisterUserPostController } from "../users/infrastructure/register.user.post.controller";
import { getAllPostsController } from "../posts/infrastructure/getAllPosts.controller"
import { AddActionUserInPost } from "../posts/infrastructure/addActionUserInPost";

const CONTROLLERS = {
    LoginPostController,
    ShowAllProductsByUserGetController,
    ShowAllTransactionsByProductPostController,
    ShowDetailTransactionGetController,
    RequestProductPostController,
    RegisterUserPostController,
    getAllPostsController,
    AddActionUserInPost
};

export class ControllerFactory {
    static createInstance(name) {        
        const cConstructor = CONTROLLERS[name];
        return cConstructor ? new cConstructor(name) : null;        
    }
}