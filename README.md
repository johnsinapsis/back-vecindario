# Vecindario
## Prueba técnica Node.js - Typescript - Arquitectura Hexagonal - Principios SOLID
Para lanzar en **_modo desarrollo_**
#### Dev Mode 
```shell script
npm run dev
```
### Pre-requisitos 📋

_Para poner en funcionamiento el proyecto se debe tener contar con una instancia de MariaDB 10.2 en adelante_
_En el directorio pre-install se encuenta un script que se debe ejecutar en un repositorio limpio. Allí se podrán encontrar datos de prueba para relizar el set de pruebas unitarias_

## Pruebas Unitarias
Para lanzar  **_pruebas unitarias_**
#### Unit Test 
```shell script
npm run test
```
_Estas pruebas permiten observar el funcionamiento de las validaciones de formato y negocio de los casos de uso. Se realizan casos de prueba tanto para los flujos principal como para el flujo alternativo_

## Configuración ⚙️

_En la ruta /src/config/config.ts encontrará el archivo de configuración de los endpoint. Puede hacer uso de un fichero .env para agregar las variables de entorno. puede ver un ejemplo en la raíz de proyecto en el fichero .env.sample_

## Notas importantes 📖
* Este código es una base que aplica arquitecturas limpias separando en capas los casos de uso y usando principios SOLID. Este código es susceptible de mejora por lo que se realizarán algunas mejoras a medida que sean detectadas. 
* Quedó pendiente la documentación swagger para los endpoint

## Autor ✒️

* **John Jairo González** - *Desarrollador Backend Node.js* - [johnsinapsis](https://gitlab.com/johnsinapsis)

## Licencia 📄

Este proyecto es una prueba técnica y es libre para que sea una base para mejorar los principios de cleanCOde y Clear Arquitecture así como Principios SOLID


